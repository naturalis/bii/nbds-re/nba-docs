---
title: Introduction
hide:
  - toc
---

## Background

The Netherlands Biodiversity API (NBA) facilitates access to the Natural History Collection at the Naturalis Biodiversity Center. Next to than museum specimen records and metadata, access to taxonomic classification and nomenclature, geographical information, and to multimedia files is provided. By using the powerful Elasticsearch engine, the NBA facilitates searching for collection- and biodiversity data in near real-time. Furthermore, by incorporating information from taxonomic databases, taxonomic name resolution can be accomplished with the NBA.  Persistent Uniform Resource Identifiers (PURLs) ensure that each species accessible via the NBA is represented by a citable unambiguous web reference. Access to our data is provided via a REST interface and several clients such as the [BioPortal](http://bioportal.naturalis.nl/"), a web application for browsing biodiversity data that is served by the NBA.

<figure>
  <div style="text-align: center;"><p>
    <img src="../images/overview.png" align="center" alt="overview" width=500>
    <figcaption>NBA overview: From data sources to end users (Icons credit: Freepik from www.flaticon.com)</figcaption>
  </div>
</figure>


## Available Data Types
The NBA provides access to four basic data types that are interlinked:

* A **Specimen** record can represent a biological (botanical or zoological) or geological entity stored at Naturalis. In addition, sound records from the wildlife sound database [Xeno-canto](https://www.xeno-canto.org/) are also stored as specimen objects. Specimen data usually comprise information on reservation, identification, classification, taxonomy (see below), and details about the gathering/acquisition process of the specimen, such as geospatial information. Specimen data are harvested from two in-house collection registration systems for animal and plant specimens, and from the wildlife sounds database [Xeno-canto](https://www.xeno-canto.org/) (also hosted at Naturalis).

* **Taxonomic** information about a biological entity is provided in Taxon the taxon data type, comprising hierarchy and placement in the Tree of Life, information on synonymy and the mapping to common (species) names in different languages. These data are taken from three taxonomic databases, the [Catalogue of Life](http://www.catalogueoflife.org/), the [Dutch Species Register (NSR)](http://www.nederlandsesoorten.nl/), a comprehensive classification of all species in the Netherlands, and the [Dutch Caribbean Species Register](https://www.dutchcaribbeanspecies.org/), an overview of the biodiversity of Aruba, Bonaire, Curaçao, Saba, Sint Eustatius and Sint Maarten. Taxon and Specimen data types can be aggregated on fields such as scientific or common taxon names.

* **Multimedia** data store images (such as photos, drawings or sonograms), videos, and sounds that are associated with Specimen and Taxon data. Data sources are our in-house collection registration systems, the wildlife sounds database [Xeno-canto](https://www.xeno-canto.org/) and the [NSR](http://www.nederlandsesoorten.nl/).

* Objects of type **GeoArea** refer to geographical regions coded as polygons in the GeoJSON format.  The NBA thereby facilitates geographical searches such as searching for all specimens that were collected in a certain region, or, vice versa, retrieve the region (e.g. a specific nature reserve) where a specimen was found. Data sources are listed in the [detailed service description](/doc-spec-services/geo/#data-source-systems).


## Services Summary

The services provided by the NBA can be roughly categorised into the following categories:

* **Query services** provide access to all indexed (and therefore searchable) fields within a data type. Simple queries can be written as [*human readable queries*](/quickstart/#human-readable) which means the query parameters are simply passed as URL parameters. [*Complex JSON queries*](/advanced-queries/#queryspec) provide a more powerful mechanism to access the data, since query terms can be nested, weighted and/or filtered in a more sophisticated manner.

* **Data access services** allow the access of specific fields within a data type. These fields are generally identifier fields. Note that all data access services are essentially query services for a certain value, implemented for a more convenient user experience.

* **Type specific metadata services** give information about data type specific fields and settings. This includes information about available fields and paths and the supported operators for comparison.

* **General metadata services** provide general information not specific to document types. This includes general settings and controlled vocabularies for certain field values.

* **Download services** facilitate the bulk retrieval of query results as [Darwin Core Archive(DwCA)](https://en.wikipedia.org/wiki/Darwin_Core_Archive) files.

* **Aggregation services** summarise a list of query results based on values that are shared for certain fields. Currently, this grouping is supported for specimen and taxon data types and the grouping is done on the scientific name.


A complete list of services can be found in the swagger_ui_link("API endpoint reference").  The table below lists the NBA's different service types and gives links to specific documentation resources and examples.

<center>

| Service type | Document type | | | |
|---|---|---|---|---|
|   | **Specimen**  | **Taxon**  | **GeoArea**  | **Multimedia**  |
| [**Query**](/quickstart/#human-readable)<br>[**Query (advanced)**](/advanced-queries/#queryspec) | Path:<br> /specimen/query/<br> {{ table_links("specimen", "queryHttpGet_2") }} <br> [Examples](/doc-spec-services/specimen)  | Path:<br> /taxon/query/<br> {{ table_links("taxon", "queryHttpGet_3") }} <br> [Examples](/doc-spec-services/taxon)  | Path: <br> /geo/query/<br> {{ table_links("geo","queryHttpGet") }} <br>[Examples](/doc-spec-services/geo) |  Path: <br> /multimedia/query/<br> {{ table_links("multimedia", "queryHttpGet_1") }} <br>[Examples](/doc-spec-services/multimedia)
| **Data access** | {{ table_links_sw_ex("specimen", "findByUnitID") }} <br>[Examples](/doc-spec-services/specimen) | {{ table_links_sw_ex("taxon", "find_4") }} <br>[Examples](/doc-spec-services/taxon ) | {{ table_links_sw_ex("geo", "find_1") }} <br>[Examples](/doc-spec-services/geo) | {{ table_links_sw_ex("multimedia", "find_2") }} <br>[Examples](/doc-spec-services/multimedia)
| **Download (static)**  | Path: <br> /specimen/dwca/getDataSet/<br> {{ table_links_sw_ex("specimen","dwcaGetDataSet1") }} <br>[Examples](/doc-spec-services/specimen#dl) | Path: <br> /taxon/dwca/getDataSet/ <br> {{ table_links_sw_ex("taxon", "dwcaGetDataSet2") }} <br>[Examples](/doc-spec-services/taxon#dl) | not available | not available |
| **Download (dynamic)**  | Path: <br> /specimen/dwca/query/<br> {{ table_links_sw_ex("specimen", "dwcaQuery") }} <br>[Examples](/doc-spec-services/specimen#dl) | Path: <br> /taxon/dwca/query/<br> {{ table_links_sw_ex("taxon", "dwcaQueryHttpGet") }} <br>[Examples](/doc-spec-services/taxon#dl) | not available | not available |
| **Metadata (data specific)** | Path: <br>/specimen/metadata/ <br> {{ table_links_sw_ex("specimen", "getPaths_2") }} <br>[Examples](/doc-spec-services/specimen#md) | Path: <br>/taxon/metadata/ <br> {{ table_links_sw_ex("taxon","getPaths_3") }} <br>[Examples](/doc-spec-services/taxon#md) | Path: <br>/geo/metadata/  <br> {{ table_links_sw_ex("geo","getPaths") }} <br>[Examples](/doc-spec-services/geo#md) | Multimedia: <br>/multimedia/metadata/  <br> {{ table_links_sw_ex("multimedia","getPaths_1") }} <br>[Examples](/doc-spec-services/multimedia#md) |
| **Metadata (general)** | Path: /metadata/ <br> {{ swagger_metadata_link("Services details") }}<br>[Examples](/doc-spec-services/metadata) | | | |
| **Aggregation** | Path: <br> /specimen/groupByScientificName/ {{ table_links_sw_ex("specimen","groupByScientificName_GET1") }} <br>[Examples](/advanced-queries/#agg) | Path: <br> /taxon/groupByScientificName/ {{ table_links_sw_ex("taxon","groupByScientificName_GET2") }} <br>[Examples](/advanced-queries/#agg) | not available | not available |

</center>

## Access to Data

### REST

The NBA is implemented as a [RESTful](https://en.wikipedia.org/wiki/Representational_state_transfer) API and can thus be accessed using standard REST clients or a web browser. For exploration or testing of the NBA services and/or PURL services we recommend using a command-line tool for transferring data as e.g. [curl](https://curl.haxx.se/) or a rest client browser plugin, e.g. the [chrome rest client](https://advancedrestclient.com/). A summary of available REST endpoints and data models present in the NBA can be found {{ swagger_ui_link("here") }}.


### API Clients

To provide programmatic access to the NBA, Naturalis plans to develop clients for several programming languages. These clients are build upon the NBA REST architecture and facilitate the integration of NBA access within scripts or software applications.  Currently, clients for the following languages are available:

* [Java](https://gitlab.com/naturalis/bii/nbds/naturalis_data_api). See also the
[javadoc](https://naturalis.gitlab.io/bii/nba/naturalis_data_api/nl/naturalis/nba/client/package-summary.html).

* [R](https://github.com/ropensci/nbaR), or [here](https://ropensci.github.io/nbaR/).

* [PHP](https://gitlab.com/naturalis/bii/nbds/bioportal-php-client)


### Bioportal

The [Bioportal](https://bioportal.naturalis.nl/) provides easy access via a web interface from which the user can browse and query the Naturalis collection data. The Bioportal makes use of (a part of) the NBA services to retrieve and display the data.
