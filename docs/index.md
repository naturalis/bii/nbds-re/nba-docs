---
title: User Documentation 
---

Welcome to the user documentation of the Netherlands Biodiversity Data Services: a collection of public services for retrieving biodiversity related data from one of the largest natural history collections in the world, present  at the [Naturalis Biodiversity Center](https://www.naturalis.nl) in Leiden, the Netherlands. Our services provide access to more than **48 million** specimen and occurrence records and counts are steadily growing as our large-scale digitization project proceeds. 


## Resources

### Web Services

The Netherlands Biodiversity Data Services are comprised of two main service components:

* The [Netherlands Biodiversity API (NBA)](/introduction) is the primary gateway to accessing the digitised Natural History collection at Naturalis.

* The [Persistent Uniform Resource Locator (PURL)](/purl-services) service provides permanent, unique and therefore citeable identifiers for all our records.


### Tools

We have developed several tools to ease access to our data:

* the [Bioportal](http://bioportal.naturalis.nl/) is a web application to conveniently browse, query and search our data;

* [client libraries](/introduction/#api-clients) provide programmatic access to the NBA;

* the {{ scratchpad_link("NBA query scratchpad") }} is an online tool for designing and running complex NBA queries.


### Further Documentation

* The {{ swagger_ui_link("NBA API endpoint reference") }} site provides detailed information about all NBA services and data models


## Version

The current version of the NBA is `v2`. The previous version (`v0`) has been deprecated since May 2018. 
