---
title: Metadata Services
---

Metadata services provide miscellaneous information about the data available via
the NBA. For a full list of services, please refer to the {{ swagger_ui_link("API endpoint reference") }}. Metadata services can be categorised into two classes:


## Controlled Vocabularies

To maintain data integrity, some fields in our source systems are constrained by controlled vocabularies. These exist for the fields `PhaseOrStage`, `Sex`, `SpecimenTypeStatus` (Specimens), `TaxonomicStatus` (Taxa). The endpoint `/medata/getControlledList/{field}` returns the allowed values for this field, e.g. for the type status:

{{ nba_link("metadata/getControlledList/SpecimenTypeStatus") }}

Allowed formats for date fields can be retrieved with {{ swagger_ui_path_link("/metadata/getAllowedDateFormats","#/metadata/getAllowedDateFormats") }}. The service {{ swagger_ui_path_link("/metadata/getRestServices","#/metadata/getRestServices") }} gives a list of all available NBA endpoints.


## Type-specific Metadata

These services reside under `/{dataType}/metadata` and give information about the field structure of the data type, such as all available paths (`getPaths`) or detailed information about data types, allowed operators and whether a field is indexed or not (`getFieldInfo`), for example:

{{ nba_link("multimedia/metadata/getPaths") }}

{{ nba_link("multimedia/metadata/getFieldInfo") }}
