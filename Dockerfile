FROM squidfunk/mkdocs-material:8.1.8
LABEL maintainer "Naturalis <info@naturalis.nl>"

RUN pip install --no-cache-dir "mkdocs-macros-plugin>=0.6"

# Set working directory
WORKDIR /docs

COPY . .

# Expose MkDocs development server port
# EXPOSE 8000

# Start development server by default
ENTRYPOINT ["mkdocs"]
CMD ["build"]
