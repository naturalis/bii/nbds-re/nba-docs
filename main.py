"""
MkDocs Macros Module
"""

import os
import html

"""
This hook adds a forward slash ('/') to the url string given, when
it not already ends with a forward slash. If the parameter is null 
or empty, no forward slash will be added.
"""
def end_url_with_forward_slash(url):
  url = url.strip()  
  if not (url is None or len(url) == 0):
    if not url.endswith("/"):
      url += "/"
  return url


"""
A set of required environment variables
"""
scratchpad_url = end_url_with_forward_slash(os.getenv('SCRATCHPAD_URL'))
swagger_ui_url = end_url_with_forward_slash(os.getenv('SWAGGER_UI_URL'))
nba_base_url   = end_url_with_forward_slash(os.getenv('NBA_BASE_URL')) + "v2/"


"""
This is the hook for defining variables, macros and filters

- variables: the dictionary that contains the environment variables
- macro: a decorator function, to declare a macro.
- filter: a function with one of more arguments,
    used to perform a transformation
"""
def define_env(env):

  """
  Hook for providing a link to the NBA. The base url must be present 
  as environment variable NBA_BASE_URL. If parameter <link_text> is given,
  this will be the link text of the hyperlink.
  """
  @env.macro
  def nba_link(path=None):
    
    if path is None or len(path) == 0:
      url = nba_base_url
    else:
      url = nba_base_url + path
    
    HTML = """<p><a href="%s" target="_blank">%s</a></p>"""
    return HTML % (url, url)

  """
  Hook for providing (part of) the URL of the NBA as text (not a hyperlink!)
  """
  @env.macro
  def nba_link_text(path=None):
    if path is None or len(path) == 0:
      return nba_base_url
    else:
      return nba_base_url + path


  """
  Hook for providing a scratchpad link based on environment variables
  """
  @env.macro
  def scratchpad_link(link_text=None):

    if link_text is None or len(link_text) == 0:
      link_text = scratchpad_url
    
    HTML = """<a href="%s" target="_blank">%s</a>"""
    return HTML % (scratchpad_url, link_text)


  """
  Hook for providing (part of) a the URL of the scratchpad as text (not a hyperlink!)
  """
  @env.macro
  def scratchpad_link_text(path=None):
    if path is None or len(path) == 0:
      return scratchpad_url
    else:
      return scratchpad_url + path

  
  """
  Hook for providing a link to the swagger UI, based on its environment variable. 
  If the parameter <link_text> is given, this will be the link text.
  """
  @env.macro
  def swagger_ui_link(link_text=None):
    
    if link_text is None or len(link_text) == 0:
      link_text = swagger_ui_url
    
    HTML = """<a href="%s" target="_blank">%s</a>"""
    return HTML % (swagger_ui_url, link_text)


  """
  Hook for providing a link to the swagger UI, based on its environment variable.
  Parameter <link_text> needs to be set with the desired link text, and 
  <swagger_path> with the extra path details.
  """
  @env.macro
  def swagger_ui_path_link(link_text, swagger_path):

    HTML =  """<a href="%s" target="_blank">%s</a>"""
    return HTML % (swagger_ui_url + swagger_path, link_text)


  """
  Provides a link to the section metadata of the Swagger documentation.
  """
  @env.macro
  def swagger_metadata_link(link_text):
    HTML = """<a href="%s"/#/metadata target="_blank">%s</a>"""
    return HTML % (swagger_ui_url, link_text)


  """
  Hook that creates a hyperlink to the  swagger UI. This hook is needed for
  the table on the Introduction page.
  """
  @env.macro
  def table_links_sw_ex(doc_type, name):
    HTML = """<a href="%s#/%s/%s" target="_blank">Service details</a>"""
    return HTML % (swagger_ui_url ,doc_type, name)


  """
  Hook that creates a block of links for each data type. This hook is needed for 
  the table on the Introduction page.
  """
  @env.macro
  def table_links(doc_type, name):  
    HTML =  """<a href="%s/#/%s/%s" target="_blank">Service details</a>""" % (swagger_ui_url, doc_type, name)
    HTML += """<br>"""
    HTML += """<a href="%s/v2/%s/metadata/getPaths" target="_blank">Search fields</a>""" % (nba_base_url, doc_type)
    return HTML


  """
  EXAMPLE: Jinja2 filter example

  Usage:

  {{ "lower" | upper }}
  
  """
  @env.filter
  def upper(str):
    "Make the string uppercase"
    return str.upper()

